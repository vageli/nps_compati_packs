# NPS Compatibility Packs

Decrypter made by dots-tb and CelesteBlue

## Requirement

- [repatch](https://github.com/dots-tb/rePatch-reDux0/releases)

## Usage

download the pack you want and extract it to `ux0:repatch/<TITLE ID>`

ex. `PCSG01030-03_670-01_00-01_01.ppk` is the file you downloaded. extract it to `ux0:repatch/PCSG01030`

*Note:* use the version indicate by your game(you can check in the game bubble),patches packs will come soon

*Note 2:* make sure you are using the latest repatch

## Usage for Patch Packs

### 3.60 Under Games with 3.61+ Patch

just use it like any other base game packs

### 3.61+ Games

1) download the base game pack, extract and put it in repatch

2) do the same for the patch pack and overwrite the files

__Note:__ when a game have new version, it will get replace, to save space(especially with games like Phantasy Star Online 2)

## Structure

.ppk are zip files with different extension

the structure follow `TITLEID-FF_FFF-XX_XX-YY_YY.ppk` , ex. `PCSG01030-03_670-01_00-01_01.ppk`

- `TITLEID : the title id of the game(ex. PCSG01030)`
- `FF_FFF : Firmware version of the game (ex. 03_670)`
- `XX_XX : App version of the game (ex. 01_00)`
- `YY_YY : Version of the game (ex. 01_01)`

## Entries.txt

there is a file [entries.txt](./entries.txt) that contain all the packs entries, this file is mostly for use with PKGj/NPS Browser

## To Submit new packs

Please use Pull Request to submit a new pack, make sure you include the sha256 files as well

## Disclaimer

These packs are for the purpose of allowing modders to learn how the game execute and work
